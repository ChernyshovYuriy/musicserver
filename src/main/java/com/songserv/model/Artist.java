package main.java.com.songserv.model;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 * Co-author: Yuriy Chernyshov
 */
public class Artist {

    private int id;
	private String name;

    /**
     * A model that holds information about concrete Artist
     *
     * @param id   unique identifier of the Artist
     * @param name name of the Artist
     */
	public Artist(int id, String name) {
		this.id = id;
		this.name = name;
	}

    /**
     * @return unique identifier of the Artist
     */
	public int getId() {
		return id;
	}

    /**
     * @return name of the Artist
     */
	public String getName() {
		return name;
	}
}