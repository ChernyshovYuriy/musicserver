package main.java.com.songserv.model;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 * Co-author: Yuriy Chernyshov
 */
public class Song {

	private int id;
	private String name;

    /**
     * A model that holds information about concrete Song
     *
     * @param id   unique identifier of the Song
     * @param name name of the Song
     */
	public Song(int id, String name) {
		this.id = id;
		this.name = name;
	}

    /**
     * @return unique identifier of the Song
     */
	public int getId() {
		return id;
	}

    /**
     * @return name of the Song
     */
	public String getName() {
		return name;
	}
}