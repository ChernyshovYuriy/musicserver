package main.java.com.songserv.util;

/**
 * Created by Dmitry Gurinovich on 2014-06-24.
 */
public interface Formatter<T> {

	public String format(T object);
}
