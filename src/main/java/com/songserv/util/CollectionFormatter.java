package main.java.com.songserv.util;

import java.util.Iterator;

/**
 * Created by Dmitry Gurinovich on 2014-06-24.
 */
public class CollectionFormatter<T> {

    private Formatter<T> itemFormatter;

    public CollectionFormatter() {

    }

    public CollectionFormatter(Formatter<T> itemFormatter) {
        this.itemFormatter = itemFormatter;
    }

    public String format(Iterable<T> collection) {
        StringBuilder stringBuilder = new StringBuilder();

        Iterator<T> iterator = collection.iterator();

        while (iterator.hasNext()) {
            T item = iterator.next();

            String formattedItem = itemFormatter != null ? itemFormatter.format(item) : item.toString();
            stringBuilder.append(formattedItem);
            stringBuilder.append('\n');
        }

        return stringBuilder.toString();
    }
}
