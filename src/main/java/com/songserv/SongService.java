package main.java.com.songserv;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 * Co-author: Yuriy Chernyshov
 */

/**
 * Main interface of the application. It allows to process input arguments as a String and return a String as result
 * of the concrete Command execution
 */
public interface SongService {

    /**
     * Method which process input arguments
     *
     * @param input input arguments, may be separated by the white space
     *
     * @return a result of the Command execution as string
     *
     * @throws Exception
     */
	public String processCommand(String input) throws Exception;
}
