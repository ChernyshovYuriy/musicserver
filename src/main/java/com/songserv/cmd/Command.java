package main.java.com.songserv.cmd;

import main.java.com.songserv.data.DataProvider;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 * Co-author: Yuriy Chernyshov
 */

/**
 * Abstract class that provide a core functionality for all it's inheritors (in particular any concrete Command)
 */
public abstract class Command {

    /**
     * Method that trigger to execute a Command
     *
     * @param dataProvider provider of the incoming data
     *
     * @return a String which represent an abstract result of the Command execution
     */
	public abstract String execute(DataProvider dataProvider);
}
