package main.java.com.songserv.cmd;

import main.java.com.songserv.data.DataProvider;
import main.java.com.songserv.model.Artist;
import main.java.com.songserv.util.CollectionFormatter;
import main.java.com.songserv.util.Formatter;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 */
public class ListArtistsCommand extends Command {

    public static final String Name = "LIST_ARTISTS";

    // TODO: Builder can be made as public not nested class, this would allow custom formatter injection
    private static class ListArtistsCommandBuilder implements CommandBuilder<ListArtistsCommand> {

        @Override
        public ListArtistsCommand create(String... args) {

            ListArtistsCommand listArtistsCommand = new ListArtistsCommand();
            listArtistsCommand.formatter = new CollectionFormatter<Artist>(new Formatter<Artist>() {
                @Override
                public String format(Artist artist) {
                    return String.format("%d %s", artist.getId(), artist.getName());
                }
            });
            return listArtistsCommand;
        }
    }

    private static ListArtistsCommandBuilder commandBuilder = new ListArtistsCommandBuilder();

    public static CommandBuilder<ListArtistsCommand> getBuilder() {
        return commandBuilder;
    }

    private CollectionFormatter<Artist> formatter;

    @Override
    public String execute(DataProvider dataProvider) {
        return formatter.format(dataProvider.getArtists());
    }
}
