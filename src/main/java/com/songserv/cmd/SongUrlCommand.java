package main.java.com.songserv.cmd;

import main.java.com.songserv.data.DataProvider;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 */
public class SongUrlCommand extends Command {

	public static final String Name = "SONG_URL";

	// TODO: Builder can be made as public not nested class
	private static class SongUrlCommandBuilder implements CommandBuilder<SongUrlCommand>{

		@Override
		public SongUrlCommand create(String... args) {

			if(args.length != 1) throw new IllegalArgumentException("SongUrl command must have song id argument");

			int songId = Integer.parseInt(args[0]);
			return new SongUrlCommand(songId);
		}
	}

	private static SongUrlCommandBuilder commandBuilder = new SongUrlCommandBuilder();

	public static CommandBuilder<SongUrlCommand> getBuilder()
	{
		return commandBuilder;
	}

	private int songId;

	public SongUrlCommand(int songId) {
		this.songId = songId;
	}

	@Override
	public String execute(DataProvider dataProvider) {
		return dataProvider.getSongUrl(songId);
	}
}
