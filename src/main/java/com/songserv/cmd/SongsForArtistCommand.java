package main.java.com.songserv.cmd;

import main.java.com.songserv.data.DataProvider;
import main.java.com.songserv.model.Song;
import main.java.com.songserv.util.CollectionFormatter;
import main.java.com.songserv.util.Formatter;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 */
public class SongsForArtistCommand extends Command {

	public static final String Name = "SONGS_FOR_ARTIST";

	// TODO: Builder can be made as public not nested class, this would allow custom formatter injection
	private static class SongsForArtistCommandBuilder implements CommandBuilder<SongsForArtistCommand>{

		@Override
		public SongsForArtistCommand create(String... args) {

			if(args.length != 1) throw new IllegalArgumentException("SongsForArtist command must have artist id argument");

			int artistId = Integer.parseInt(args[0]);
			SongsForArtistCommand songsForArtistCommand = new SongsForArtistCommand(artistId);
			songsForArtistCommand.formatter = new CollectionFormatter<Song>(new Formatter<Song>() {
				@Override
				public String format(Song song) {
					return String.format("%d %s", song.getId(), song.getName());
				}
			});
			return songsForArtistCommand;
		}
	}

	private static SongsForArtistCommandBuilder commandBuilder = new SongsForArtistCommandBuilder();

	public static CommandBuilder<SongsForArtistCommand> getBuilder()
	{
		return commandBuilder;
	}

	private int artistId;

	public SongsForArtistCommand(int artistId) {
		this.artistId = artistId;
	}

	private CollectionFormatter<Song> formatter;

	@Override
	public String execute(DataProvider dataProvider) {
		return formatter.format(dataProvider.getArtistsSongs(artistId));
	}
}
