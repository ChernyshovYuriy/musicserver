package main.java.com.songserv.cmd;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 * Co-author: Yuriy Chernyshov
 */

/**
 * Interface that describe a capabilities of the {@link main.java.com.songserv.cmd.CommandBuilder} implementation
 * @param <T>
 */
public interface CommandBuilder<T extends Command> {

    /**
     * Method which allows to create a {@link main.java.com.songserv.cmd.Command}
     *
     * @param args input parameters
     *
     * @return a created {@link main.java.com.songserv.cmd.Command}
     *
     * @throws Exception
     */
	T create(String... args) throws Exception;
}
