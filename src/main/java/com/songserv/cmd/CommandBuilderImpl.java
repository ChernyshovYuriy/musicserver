package main.java.com.songserv.cmd;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 * Co-author: Yuriy Chernyshov
 */
public class CommandBuilderImpl implements CommandBuilder<Command> {

    private HashMap<String, CommandBuilder> commandMap = new HashMap<String, CommandBuilder>();

    public CommandBuilderImpl() {
        // TODO: all available commands can be registered with reflection of DI framework
        commandMap.put(ListArtistsCommand.Name, ListArtistsCommand.getBuilder());
        commandMap.put(SongsForArtistCommand.Name, SongsForArtistCommand.getBuilder());
        commandMap.put(SongUrlCommand.Name, SongUrlCommand.getBuilder());
    }

    @Override
    public Command create(String... input) throws Exception {

        if (!commandMap.containsKey(input[0])) throw new Exception("Unknown command");

        CommandBuilder commandBuilder = commandMap.get(input[0]);

        String[] args = Arrays.copyOfRange(input, 1, input.length - 1);

        return commandBuilder.create(args);
    }
}
