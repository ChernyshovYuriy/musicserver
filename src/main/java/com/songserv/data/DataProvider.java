package main.java.com.songserv.data;

import main.java.com.songserv.model.Artist;
import main.java.com.songserv.model.Song;

import java.util.List;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 */
public interface DataProvider {
	List<Artist> getArtists();
	List<Song> getArtistsSongs(int artistId);
	String getSongUrl(int songId);
}
