package main.java.com.songserv;

import main.java.com.songserv.cmd.Command;
import main.java.com.songserv.cmd.CommandBuilder;
import main.java.com.songserv.data.DataProvider;

/**
 * Created by Dmitry Gurinovich on 2014-06-23.
 * Co-author: Yuriy Chernyshov
 */
public class SongServiceImpl implements SongService {

    private final CommandBuilder commandBuilder;
    private final DataProvider dataProvider;

    public SongServiceImpl(CommandBuilder commandBuilder, DataProvider dataProvider) {
        if (commandBuilder == null) throw new IllegalArgumentException("commandBuilder");
        if (dataProvider == null) throw new IllegalArgumentException("dataProvider");

        this.commandBuilder = commandBuilder;
        this.dataProvider = dataProvider;
    }

    @Override
    public String processCommand(String input) throws Exception {
        Command command = commandBuilder.create(input.split(" "));
        return command.execute(dataProvider);
    }
}
