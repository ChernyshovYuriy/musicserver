package main.java.com.songserv.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SongTest {

    private static final String SONG_NAME = "SongName";
    private static final int SONG_ID = 987654321;

    private Song song;

    @Before
    public void setUp() throws Exception {
        song = new Song(SONG_ID, SONG_NAME);
    }

    @Test
    public void testGetId() throws Exception {
        assertEquals(SONG_ID, song.getId());
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals(SONG_NAME, song.getName());
    }
}