package main.java.com.songserv.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArtistTest {

    private static final String ARTIST_NAME = "ArtistName";
    private static final int ARTIST_ID = 123456789;

    private Artist artist;

    @Before
    public void setUp() throws Exception {
        artist = new Artist(ARTIST_ID, ARTIST_NAME);
    }

    @Test
    public void testGetId() throws Exception {
        assertEquals(ARTIST_ID, artist.getId());
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals(ARTIST_NAME, artist.getName());
    }
}